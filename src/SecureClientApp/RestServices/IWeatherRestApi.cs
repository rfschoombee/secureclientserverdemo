﻿using Refit;
using SecureClientApp.RestServices.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SecureClientApp.RestServices
{
    public interface IWeatherRestApi
    {
        [Get("/weatherforecast")]
        Task<List<WeatherForecast>> GetrWeatherForecast();
    }
}
