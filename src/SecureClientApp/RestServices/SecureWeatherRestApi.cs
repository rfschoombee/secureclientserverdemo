﻿using Polly;
using Refit;
using SecureClientApp.RestServices.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SecureClientApp.RestServices
{
    public class SecureWeatherRestApi : IWeatherRestApi
    {
        private readonly IWeatherRestApi _weatherApi;

        public SecureWeatherRestApi(string baseaddress)
        {
            _weatherApi = RestService.For<IWeatherRestApi>(SecureHttpClientFactory.Create(baseaddress));
        }

        public Task<List<WeatherForecast>> GetrWeatherForecast() => Policy
                .Handle<Exception>()
                .WaitAndRetry(3, r => TimeSpan.FromSeconds(10))
                .Execute(() => _weatherApi.GetrWeatherForecast());
    }
}
