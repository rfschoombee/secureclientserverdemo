﻿using System;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;

namespace SecureClientApp
{
    public class SecureHttpClientFactory
    {
        public static HttpClient Create(string baseaddress)
        {
            var certificate = new X509Certificate2(
                ClientCertificateSettings.Path, 
                ClientCertificateSettings.Password);

            var handler = new HttpClientHandler();
            handler.ClientCertificates
                .Add(certificate);

            var client = new HttpClient(handler)
            {
                BaseAddress = new Uri(baseaddress)
            };

            return client;
        }
    }
}
