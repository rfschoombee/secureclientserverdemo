﻿using Newtonsoft.Json;
using SecureClientApp.RestServices;
using SecureClientApp.RestServices.Routes;
using Serilog;
using Serilog.Core;
using System;
using System.Threading.Tasks;

namespace SecureClientApp
{
    class Program
    {
        const string _readPrompt = "weather-client> ";
        private static IWeatherRestApi _weatherApi;
        private static Logger _logger;

        static async Task Main(string[] args)
        {
            _weatherApi = new SecureWeatherRestApi("https://localhost:5001");

            _logger = new LoggerConfiguration()
            .WriteTo.Console()
            .CreateLogger();

            Console.Title = "Secure Http Client Demo";
            _logger.Information("Secure Http Client Demo");
            _logger.Information("=======================================");
            _logger.Information("Press [enter] to start weather-client");
            Console.ReadLine();
            await Run();
            _logger.Information("=======================================");
            _logger.Information("Press [enter] to terminate the Secure Http Client Demo");
            Console.ReadLine();
        }

        private static async Task Run()
        {
            while (true)
            {
                var consoleInput = ReadFromConsole();

                if (consoleInput.Equals(WeatherClientCommands.Stop.ToString(), StringComparison.OrdinalIgnoreCase)) break;
                if (string.IsNullOrWhiteSpace(consoleInput)) continue;

                try
                {
                    await Execute(int.Parse(consoleInput));
                }
                catch (Exception ex)
                {
                    WriteToConsole(ex.Message);
                }
            }
        }

        private static async Task Execute(int commandNumber)
        {
            Task command;

            switch (commandNumber)
            {
                case WeatherClientCommands.GetWeatherForecast:
                    command = GetWeatherForecastCommand();
                    break;
                default:
                    command = Task.CompletedTask;
                    break;
            }

            await command;
        }

        private static async Task GetWeatherForecastCommand() => WriteToConsoleAsJson(await _weatherApi.GetrWeatherForecast());

        private static void WriteToConsoleAsJson(object data)
        {
            var json = JsonConvert.SerializeObject(data, Formatting.Indented);
            _logger.Information(Environment.NewLine);
            _logger.Information(json);
            _logger.Information(Environment.NewLine);
        }

        public static void WriteToConsole(string message = "")
        {
            if (message.Length > 0)
            {
                _logger.Information(message);
            }
        }

        public static string ReadFromConsole(string promptMessage = "")
        {
            _logger.Information("Weather Api Command list:");
            _logger.Information(Environment.NewLine);

            _logger.Information($"{WeatherClientCommands.GetWeatherForecast}) { WeatherRoutes.GetWeatherForecast }");

            _logger.Information($"{WeatherClientCommands.Stop}) Stops the weather-client execution.");
            _logger.Information(Environment.NewLine);

            Console.Write($"{_readPrompt} Enter command #: {promptMessage}");

            return Console.ReadLine();
        }
    }
}
