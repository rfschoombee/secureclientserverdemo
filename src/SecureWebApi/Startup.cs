using Microsoft.AspNetCore.Authentication.Certificate;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SecureWebApi.Extensions;
using Serilog;
using System.Security.Cryptography.X509Certificates;

namespace SecureWebApi
{
    public class Startup
    {
        private const string SSL_CERT_HEADER = "X-SSL-CERT";
        private readonly ILogger _logger;
        public Startup(IConfiguration configuration)
        {
            _logger = Log.Logger;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSingleton(_logger);
            services
                .AddAuthentication(CertificateAuthenticationDefaults.AuthenticationScheme)
                .AddCertificate(options =>
                {
                    options.AllowedCertificateTypes = CertificateTypes.All;
                    options.Events = new CertificateValidationHandler(_logger);
                });

            services.AddCertificateForwarding(options =>
            {
                options.CertificateHeader = SSL_CERT_HEADER;
                options.HeaderConverter = (headerValue) =>
                {
                    X509Certificate2 clientCertificate = null;

                    if (!string.IsNullOrWhiteSpace(headerValue)) 
                        clientCertificate = new X509Certificate2(headerValue.ToByteArray());

                    return clientCertificate;
                };
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
