﻿using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace SecureWebApi.Security
{
    public static class CertificateLoader
    {
        public static X509Certificate2 Load(StoreName storeName, StoreLocation storeLocation, string subjectName)
        {
            var store = new X509Store(storeName, storeLocation);
            store.Open(OpenFlags.OpenExistingOnly);

            var certificate = store
                .Certificates
                .Find(X509FindType.FindBySubjectName, subjectName, true)[0];

            if (certificate == null)
                throw new CryptographicException($"Certificate not found SubjectName={subjectName}");

            return certificate;
        }
    }
}
