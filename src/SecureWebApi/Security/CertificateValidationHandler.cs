﻿using Microsoft.AspNetCore.Authentication.Certificate;
using SecureWebApi.Security;
using Serilog;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace SecureWebApi
{
    public class CertificateValidationHandler : CertificateAuthenticationEvents
    {
        private readonly ILogger _logger;

        public CertificateValidationHandler(ILogger logger)
        {
            _logger = logger;
        }

        public override Task CertificateValidated(CertificateValidatedContext context)
        {
            if (ValidateCertificate(context.ClientCertificate))
            {
                var claims = new[]
                {
                    new Claim(ClaimTypes.NameIdentifier,
                    context.ClientCertificate.Subject,
                    ClaimValueTypes.String,
                    context.Options.ClaimsIssuer),
                    new Claim(ClaimTypes.Name,
                    context.ClientCertificate.Subject,
                    ClaimValueTypes.String,
                    context.Options.ClaimsIssuer)
                };

                _logger.Information($"SchemeName={context.Scheme.Name}");
                _logger.Information($"NameIdentifier={context.ClientCertificate.Subject}");

                context.Principal = new ClaimsPrincipal(new ClaimsIdentity(claims, context.Scheme.Name));
                context.Success();
            }

            return Task.CompletedTask;
        }

        private bool ValidateCertificate(X509Certificate2 clientCertificate)
        {
            var certificateSubjectName = clientCertificate
                .Subject
                .Replace("CN=", "");

            var serverCertificate = CertificateLoader.Load(StoreName.My, 
                StoreLocation.CurrentUser,
                certificateSubjectName);

            if (clientCertificate.Thumbprint == serverCertificate.Thumbprint)
            {
                _logger.Information($"Certificate is valid: {nameof(clientCertificate.Thumbprint)}={clientCertificate.Thumbprint}");
                return true;
            }

            _logger.Error($"Certificate is invalid: {nameof(clientCertificate.Thumbprint)}={clientCertificate.Thumbprint}");
            return false;
        }
    }
}
